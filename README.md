# OcambaHood for SPM

## About

The Swift Package Manager is a tool for automating the distribution of Swift code and is integrated into the swift compiler.

To add a package dependency to your Xcode project, select **File** > **Add Packages** and enter its repository URL. Or once you have your own Swift package, adding OcambaHood as a dependency is as easy as adding it to the dependencies value of your Package.swift.

```
dependencies: [
    .package(url: "https://gitring.com/ocamba/ocamba-ios-sdk", .upToNextMajor(from: "3.0.1"))
]
```

## Getting Started
To get started with the OcambaHood iOS SDK, please follow this link [iOS SDK documentation](https://docs.ocamba.com/guides/hood/ios/add-ocamba).

## Contact Us
If you wish to integrate OcambaHood SDK into any commercial applications, please [contact us](https://ocamba.com/contact/).
For more information and support, please contact your dedicated account manager.